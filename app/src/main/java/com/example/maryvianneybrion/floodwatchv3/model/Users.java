package com.example.maryvianneybrion.floodwatchv3.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by Mary Vianney Brion on 5/19/2017.
 */

public class Users {

    @SerializedName("last_name")
    @Expose
    private String last_name;
    @SerializedName("first_name")
    @Expose
    private String first_name;
    @SerializedName("email")
    @Expose
    private String email;
    @SerializedName("username")
    @Expose
    private String username;
    @SerializedName("password")
    @Expose
    private String password;
    @SerializedName("old_password")
    @Expose
    private String old_password;
    @SerializedName("token")
    @Expose
    private String token;
    @SerializedName("password_confirmation")
    @Expose
    private String confirm_password;
    private String final_password;
    @SerializedName("device_token")
    @Expose
    private String device_token;
    @SerializedName("newpassword")
    @Expose
    private String newpassword;
    @SerializedName("newpassword_confirmation")
    @Expose
    private String newpassword_confirmation;


    /**
     * getters
     * <p>
     * <p>
     * public String getLastn(){
     * return last_name;
     * }
     * public String getFirstn(){
     * return first_name;
     * }
     * public String getEmailad(){
     * return email;
     * }
     * public String getUsname(){
     * return username;
     * }
     * public String getUpassw(){
     * return password;
     * }
     * public String getUniqueid(){
     * return uniqueid;
     * }
     * public String getDeviceToken(){
     * return dev_token;
     * }
     * public String getConfirm_password(){
     * return confirm_password;
     * }
     * <p>
     * <p>
     * setters
     **/

    public void setLastn(String lstn) {
        this.last_name = lstn;
    }

    public void setFirstn(String frstn) {
        this.first_name = frstn;
    }

    public void setUsname(String usnme) {
        this.username = usnme;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public void setUpassw(String upassww) {
        this.password = upassww;
    }

    public void setConfirm_password(String confirm_password) {
        this.confirm_password = confirm_password;
    }

    public void setNew_password(String new_password) {
        this.password = new_password;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public void setDevice_token(String device_token) {
        this.device_token = device_token;
    }

    public void setNewpassword(String newpassword) {
        this.newpassword = newpassword;
    }

    public void setNewpassword_confirmation(String newpassword_confirmation) {
        this.newpassword_confirmation = newpassword_confirmation;
    }
}
