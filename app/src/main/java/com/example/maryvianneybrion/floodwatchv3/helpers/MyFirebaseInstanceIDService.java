package com.example.maryvianneybrion.floodwatchv3.helpers;

import android.util.Log;

import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.iid.FirebaseInstanceIdService;

/**
 * Created by Mary Vianney Brion on 5/18/2017.
 */

public class MyFirebaseInstanceIDService extends FirebaseInstanceIdService {

    private static final String TAG = "MyFirebaseIIDService";

    @Override
    public void onTokenRefresh() {
        //get registration token
        String refreshedToken = FirebaseInstanceId.getInstance().getToken();
        //display token on logcat
        Log.d(TAG, "Refreshed Token: " + refreshedToken);
        sendRegistrationToServer(refreshedToken);
    }

    private void sendRegistrationToServer(String token) {
        SharedPrefManager.saveDeviceToken(token);
    }
}
