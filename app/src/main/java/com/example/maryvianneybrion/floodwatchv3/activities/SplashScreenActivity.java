package com.example.maryvianneybrion.floodwatchv3.activities;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.view.Window;
import android.view.WindowManager;

import com.example.maryvianneybrion.floodwatchv3.R;

public class SplashScreenActivity extends Activity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        final int SPLASH_DISP_LENGTH = 1000;
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_splash_screen);

        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                Intent ii = new Intent(SplashScreenActivity.this, RegisterWithEmail.class);
                SplashScreenActivity.this.startActivity(ii);
                SplashScreenActivity.this.finish();
            }
        }, SPLASH_DISP_LENGTH);
    }
}
