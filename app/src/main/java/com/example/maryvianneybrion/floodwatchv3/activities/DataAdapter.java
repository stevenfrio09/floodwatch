package com.example.maryvianneybrion.floodwatchv3.activities;

/**
 * Created by microsdhc on 6/8/17.
 */

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.maryvianneybrion.floodwatchv3.R;

import java.util.ArrayList;

public class DataAdapter extends RecyclerView.Adapter<DataAdapter.ViewHolder> implements Filterable {
    private ArrayList<DataMonitored> dataMonitoredArray;
    private ArrayList<DataMonitored> mFilteredList;
    private ArrayList<Device> device;
    private ArrayList<Level> level;
    MonitoringHomePage monitoringHomePage;


    public DataAdapter(ArrayList<DataMonitored> dataMonitored) {
        dataMonitoredArray = dataMonitored;
        mFilteredList = dataMonitored;

    }

    @Override
    public DataAdapter.ViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.card_row, viewGroup, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(DataAdapter.ViewHolder viewHolder, int i) {

        viewHolder.tv_locationName.setText(mFilteredList.get(i).getName());
        viewHolder.tv_statusName.setText(mFilteredList.get(i).getDevice().getLevel().getStatus().toString());
        viewHolder.tv_updatedAtName.setText(mFilteredList.get(i).getDevice().getLevel().getCreatedAt().toString());
        viewHolder.tv_deviceName.setText(mFilteredList.get(i).getDevice().getName().toString());
        viewHolder.tv_heightName.setText(String.valueOf(mFilteredList.get(i).getDevice().getLevel().getHeight()));


        String statusName = viewHolder.tv_statusName.getText().toString();
        if (statusName.equalsIgnoreCase("MONITOR")) {
            viewHolder.iv_statusIcon.setImageResource(R.drawable.green);
        } else if (statusName.equalsIgnoreCase("PREPARE")) {
            viewHolder.iv_statusIcon.setImageResource(R.drawable.yellow);
        } else {
            viewHolder.iv_statusIcon.setImageResource(R.drawable.red);
        }

    }

    @Override

    public int getItemCount() {
        return mFilteredList.size();
    }


    @Override
    public Filter getFilter() {

        return new Filter() {
            @Override
            protected FilterResults performFiltering(CharSequence charSequence) {

                String charString = charSequence.toString();

                if (charString.isEmpty()) {

                    mFilteredList = dataMonitoredArray;
                } else {

                    ArrayList<DataMonitored> filteredList = new ArrayList<>();

                    for (DataMonitored filteredData : dataMonitoredArray) {

                        if (filteredData.getName().toLowerCase().contains(charString) ||
                                filteredData.getDevice().getLevel().getStatus().toLowerCase().contains(charString) ||
                                filteredData.getDevice().getLevel().getCreatedAt().toLowerCase().contains(charString) ||
                                filteredData.getDevice().getName().toLowerCase().contains(charString)) {

                            filteredList.add(filteredData);
                        }
                    }

                    mFilteredList = filteredList;
                }

                FilterResults filterResults = new FilterResults();
                filterResults.values = mFilteredList;
                return filterResults;
            }

            @Override
            protected void publishResults(CharSequence charSequence, FilterResults filterResults) {
                mFilteredList = (ArrayList<DataMonitored>) filterResults.values;
                notifyDataSetChanged();
            }
        };
    }


    public class ViewHolder extends RecyclerView.ViewHolder {
        private TextView tv_locationName, tv_statusName, tv_updatedAtName, tv_deviceName, tv_heightName;
        private ImageView iv_statusIcon;
        TextView edtmonitoringstatus;

        public ViewHolder(View view) {
            super(view);

            tv_locationName = (TextView) view.findViewById(R.id.tv_locationName);
            tv_statusName = (TextView) view.findViewById(R.id.tv_statusName);
            tv_updatedAtName = (TextView) view.findViewById(R.id.tv_updatedAtName);
            tv_deviceName = (TextView) view.findViewById(R.id.tv_deviceName);
            tv_heightName = (TextView) view.findViewById(R.id.tv_heightName);
            iv_statusIcon = (ImageView) view.findViewById(R.id.iv_statusIcon);


            edtmonitoringstatus = (TextView) view.findViewById(R.id.edtmonitoringstatus);

            view.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent = new Intent(v.getContext(), MonitoringHomePage.class);
                    Bundle bundle_info = new Bundle();
                    bundle_info.putString("locationKey", tv_locationName.getText().toString());
                    bundle_info.putString("statusKey", tv_statusName.getText().toString());
                    bundle_info.putString("updatedAtKey", tv_updatedAtName.getText().toString());
                    bundle_info.putString("deviceKey", tv_deviceName.getText().toString());
                    bundle_info.putString("heightKey", tv_heightName.getText().toString());

                    intent.putExtras(bundle_info);


                    v.getContext().startActivity(intent);
                }
            });


        }


    }


}