package com.example.maryvianneybrion.floodwatchv3.helpers;

import android.content.Intent;
import android.util.Log;

import com.example.maryvianneybrion.floodwatchv3.activities.LoginActivity;
import com.example.maryvianneybrion.floodwatchv3.activities.MonitoringHomePage;
import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;

import org.json.JSONObject;

/**
 * Created by Mary Vianney Brion on 5/19/2017.
 */

public class MyFirebaseMessagingService extends FirebaseMessagingService {
    private static final String TAG = "FirebaseMSGService";
    SharedPrefManager sharedPrefManager;

    @Override
    public void onMessageReceived(RemoteMessage remoteMessage) {
        Log.d(TAG, "From: " + remoteMessage.getFrom());

//        //if message contains data payload
//        if (remoteMessage.getData().size() > 0){
//            Log.d(TAG,"Message: "+remoteMessage.getData());
//            try{
//                JSONObject json = new JSONObject(remoteMessage.getData().toString());
//                sendPushNotification(json);
//            } catch (Exception e){
//                Log.e(TAG,"Exception: "+e.getMessage());
//            }
//        }

        if (remoteMessage.getData().size() > 0) {
            Log.d(TAG, "Message data payload: " + remoteMessage.getData());
        }
        // Check if message contains a notification payload.
        if (remoteMessage.getNotification() != null) {
            Log.d(TAG, "Message Notification Title: " + remoteMessage.getNotification().getTitle());
        }
        if (remoteMessage.getNotification() != null) {
            Log.d(TAG, "Message Notification Body: " + remoteMessage.getNotification().getBody());
        }
        String title = null;
        String message = null;
        try {
            title = remoteMessage.getData().get("title");
        } catch (Exception e) {
            title = remoteMessage.getNotification().getTitle();
        }
//        if (title==null)title = remoteMessage.getNotification().getTitle().toString();
        try {
            message = remoteMessage.getData().get("message");
        } catch (Exception e) {
            message = remoteMessage.getNotification().getBody();
        }
//        if (message==null)message = remoteMessage.getNotification().getBody().toString();


        Log.i("Title", "Message received [" + title + "]");
        Log.i("Msg", "Message received " + message + "");

        sendPushNotification(title, message);

        //if message has notification payload
        if (remoteMessage.getNotification() != null) {
            try {
                Log.d(TAG, remoteMessage.getNotification().getBody());
                Log.d(TAG, remoteMessage.getNotification().getTitle());
                JSONObject jsonObject = new JSONObject("data");
                jsonObject.put("title", remoteMessage.getNotification().getTitle());
                jsonObject.put("content", remoteMessage.getNotification().getBody());
                sendPushNotification(title, message);
            } catch (Exception e) {
                Log.e(TAG, "Exception: " + e.getMessage());
            }

            Log.d(TAG, "Message Notification Body: " + remoteMessage.getNotification().getBody());
        }
    }

    private void sendPushNotification(String title, String message) {
        // Log.e(TAG,"Notification JSON: "+jsonObject.toString());
        try {
            //JSONObject json = jsonObject.getJSONObject("data");
            //String title = json.getString("title");
            //String body = json.getString("image");
            sharedPrefManager = new SharedPrefManager(this);
            if (sharedPrefManager.isLoggedIn()) {
//                Intent i = new Intent(getApplicationContext(),MonitoringHomePage.class);
//                i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK |Intent.FLAG_ACTIVITY_NO_HISTORY );
//                startActivity(i);
                MyNotificationManager notificationManager = new MyNotificationManager(getApplicationContext());
                Intent ii = new Intent(getApplicationContext(), MonitoringHomePage.class);
                //ii.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK |Intent.FLAG_ACTIVITY_NO_HISTORY );
                notificationManager.showSmallNotification(title, message, ii);
            } else {
                Intent i = new Intent(getApplicationContext(), LoginActivity.class);
                i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_NO_HISTORY);
                startActivity(i);
            }
//        } catch (Exception e){
//            Log.e(TAG,"JSON Exception: "+e.getMessage());
        } catch (Exception e) {
            Log.e(TAG, "Exception: " + e.getMessage());
        }
    }

}
