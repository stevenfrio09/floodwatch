package com.example.maryvianneybrion.floodwatchv3.requestinterpeys;

/**
 * Created by Mary Vianney Brion on 5/19/2017.
 */

public class ServerRequest {
    private String email;
    private String password;
    private String password_confirmation;
    private String token;
    private String last_name;
    private String first_name;
    private String username;
    private String device_token;
    private String newpassword;
    private String newpassword_confirmation;

    public void setEmail(String email) {
        this.email = email;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public void setPassword_confirmation(String password_confirmation) {
        this.password_confirmation = password_confirmation;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public void setLast_name(String last_name) {
        this.last_name = last_name;
    }

    public void setFirst_name(String first_name) {
        this.first_name = first_name;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public void setDevice_token(String device_token) {
        this.device_token = device_token;
    }

    public void setNewpassword(String newpassword) {
        this.newpassword = newpassword;
    }

    public void setNewpassword_confirmation(String newpassword_confirmation) {
        this.newpassword_confirmation = newpassword_confirmation;
    }
}
