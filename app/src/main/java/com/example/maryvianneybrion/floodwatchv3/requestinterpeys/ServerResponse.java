package com.example.maryvianneybrion.floodwatchv3.requestinterpeys;

import com.example.maryvianneybrion.floodwatchv3.model.Users;

/**
 * Created by Mary Vianney Brion on 5/19/2017.
 */

public class ServerResponse {
    private String result;
    private String message;
    private String token;
    private String email;
    private Users users;

    public String getResult() {
        return result;
    }

    public String getMessage() {
        return message;
    }

    public Users getUsers() {
        return users;
    }

    public String getToken() {
        return token;
    }

    public String getEmail() {
        return email;
    }
}
