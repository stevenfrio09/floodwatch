package com.example.maryvianneybrion.floodwatchv3.activities;

import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.design.widget.CoordinatorLayout;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.example.maryvianneybrion.floodwatchv3.BuildConfig;
import com.example.maryvianneybrion.floodwatchv3.R;
import com.example.maryvianneybrion.floodwatchv3.helpers.SharedPrefManager;
import com.example.maryvianneybrion.floodwatchv3.helpers.config;
import com.example.maryvianneybrion.floodwatchv3.model.Users;
import com.example.maryvianneybrion.floodwatchv3.requestinterpeys.RequestInterfaceRegistration;
import com.example.maryvianneybrion.floodwatchv3.requestinterpeys.ServerRequest;
import com.example.maryvianneybrion.floodwatchv3.requestinterpeys.ServerResponse;

import java.util.HashMap;
import java.util.concurrent.TimeUnit;

import okhttp3.Cache;
import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class AccountSettings extends AppCompatActivity implements View.OnTouchListener {
    EditText edtEmailAddress;
    private String user_email, usertoken;
    final Context c = this;
    CoordinatorLayout coordinatorLayout;
    SwipeRefreshLayout swiper;
    TextView txtChangeName, txtToken;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_account_settings);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        //Bundle bundle = getIntent().getExtras();
        //token = bundle.getString("token");
        //user_email = bundle.getString("email");

        SharedPrefManager sharedPrefManager = new SharedPrefManager(getApplicationContext());
        HashMap<String, String> user = sharedPrefManager.getUserDetails();
        user_email = user.get(SharedPrefManager.KEY_EMAIL);
        usertoken = user.get(SharedPrefManager.KEY_TOKEN);
        //Toast.makeText(getApplicationContext(),user_email+usertoken,Toast.LENGTH_LONG).show();

        txtChangeName = (TextView) findViewById(R.id.txtChangeName);
        txtToken = (TextView) findViewById(R.id.txtToken);

        edtEmailAddress = (EditText) findViewById(R.id.editTextEmailAddress);

        swiper = (SwipeRefreshLayout) findViewById(R.id.swipeContainer);
        swiper.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                edtEmailAddress.setText(user_email);
                //txtToken.setText(token);
                swiper.setRefreshing(false);
            }
        });

        coordinatorLayout = (CoordinatorLayout) findViewById(R.id.coordinatorLayout);

        txtChangeName.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                if (event.getAction() == MotionEvent.ACTION_UP) {
                    goChangeName();
                }
                return true;
            }
        });
    }

    /**
     * update profile pop up dialog function
     **/
    private void goChangeName() {
        LayoutInflater layoutInflater = LayoutInflater.from(c);
        View mView = layoutInflater.inflate(R.layout.customdialog_account_changefname, null);
        AlertDialog.Builder alertDialogChangePassword = new AlertDialog.Builder(c);
        alertDialogChangePassword.setView(mView);

        final EditText edtlname = (EditText) mView.findViewById(R.id.userInputDialogLN);
        final EditText edtfname = (EditText) mView.findViewById(R.id.userInputDialogFN);
        final EditText edtuname = (EditText) mView.findViewById(R.id.userInputDialogUN);

        alertDialogChangePassword
                .setCancelable(false)
                .setPositiveButton("Change", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                    }
                })
                .setNegativeButton("Cancel",
                        new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                dialog.cancel();
                            }
                        });
        AlertDialog alertDialogAndroid = alertDialogChangePassword.create();
        alertDialogAndroid.show();
        alertDialogAndroid.getButton(AlertDialog.BUTTON_POSITIVE).setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                if (event.getAction() == MotionEvent.ACTION_UP) {
                    String edtlastn = edtlname.getText().toString().trim();
                    String edtfirstn = edtfname.getText().toString().trim();
                    String edtusern = edtuname.getText().toString().trim();
                    if (!edtlastn.isEmpty() && !edtfirstn.isEmpty() && !edtusern.isEmpty()) {
                        changeName(edtlastn, edtfirstn, edtusern, user_email, usertoken);
                    } else {
                        edtlname.setError("All fields are required");
                        edtfname.setError("All fields are required");
                        edtuname.setError("All fields are required");
                    }
                }
                return true;
            }
        });
    }

    /**
     * update profile main function
     **/
    private void changeName(String last_name, String first_name, String username, String email, String token) {
        OkHttpClient.Builder builder = new OkHttpClient().newBuilder();
        builder.readTimeout(60, TimeUnit.SECONDS);
        builder.connectTimeout(60, TimeUnit.SECONDS);
        builder.writeTimeout(60, TimeUnit.SECONDS);
        if (BuildConfig.DEBUG) {
            HttpLoggingInterceptor interceptor = new HttpLoggingInterceptor();
            interceptor.setLevel(HttpLoggingInterceptor.Level.BODY);
            builder.addInterceptor(interceptor);
        }
        int cacheSize = 30 * 1024 * 1024; // 10 MiB
        Cache cache = new Cache(getCacheDir(), cacheSize);
        builder.cache(cache);

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(config.URL_BASE)
                .addConverterFactory(GsonConverterFactory.create())
                .client(builder.build())
                .build();

        RequestInterfaceRegistration requestInterfaceRegistration = retrofit.create(RequestInterfaceRegistration.class);

        Users users = new Users();
        users.setEmail(email);
        users.setLastn(last_name);
        users.setFirstn(first_name);
        users.setUsname(username);

        ServerRequest serverRequest = new ServerRequest();
        serverRequest.setLast_name(last_name);
        serverRequest.setFirst_name(first_name);
        serverRequest.setEmail(email);
        serverRequest.setUsername(username);
        Call<ServerResponse> serverResponseCall = requestInterfaceRegistration.update("application/json", token, serverRequest);
        serverResponseCall.enqueue(new Callback<ServerResponse>() {
            @Override
            public void onResponse(Call<ServerResponse> call, Response<ServerResponse> response) {
                ServerResponse serverResponse = response.body();
                try {
                    if (serverResponse.getMessage().equals("Profile Updated!")) {
                        if (serverResponse.getResult().equals(config.MSG_SUCCESS)) {
                            Toast.makeText(getApplicationContext(), "Profile Updated!", Toast.LENGTH_LONG).show();
                        } else {
                            Toast.makeText(getApplicationContext(), "Error occured. Try again", Toast.LENGTH_LONG).show();
                        }
                    } else {
                        Toast.makeText(getApplicationContext(), config.MSG_FAILURE, Toast.LENGTH_LONG).show();
                    }
                } catch (Exception e) {
                    Toast.makeText(getApplicationContext(), "Error occured. Try again", Toast.LENGTH_LONG).show();
                }

            }

            @Override
            public void onFailure(Call<ServerResponse> call, Throwable t) {
                Log.d(config.TAG, "failed");
                Toast.makeText(getApplicationContext(), "There is an error occurred. Please try again", Toast.LENGTH_LONG).show();
            }
        });
    }

    @Override
    public boolean onTouch(View v, MotionEvent event) {
        return false;
    }
}
