package com.example.maryvianneybrion.floodwatchv3.activities;

import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.text.TextUtils;
import android.util.Log;
import android.util.Patterns;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.example.maryvianneybrion.floodwatchv3.BuildConfig;
import com.example.maryvianneybrion.floodwatchv3.R;
import com.example.maryvianneybrion.floodwatchv3.helpers.SharedPrefManager;
import com.example.maryvianneybrion.floodwatchv3.helpers.config;
import com.example.maryvianneybrion.floodwatchv3.model.Users;
import com.example.maryvianneybrion.floodwatchv3.requestinterpeys.RequestInterfaceRegistration;
import com.example.maryvianneybrion.floodwatchv3.requestinterpeys.ServerRequest;
import com.example.maryvianneybrion.floodwatchv3.requestinterpeys.ServerResponse;

import java.util.HashMap;
import java.util.concurrent.TimeUnit;

import okhttp3.Cache;
import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;


public class LoginActivity extends Activity implements View.OnTouchListener {

    EditText edtemailuser, edtpassword;
    Button btnLogin;
    TextView txtforgetpw, txtregisteruser;
    final Context c = this;
    private SharedPreferences pref;

    private SharedPrefManager sharedPrefManager;

    private String useremail, usertoken;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_login);

        sharedPrefManager = new SharedPrefManager(getApplicationContext());
        HashMap<String, String> user = sharedPrefManager.getUserDetails();
        useremail = user.get(SharedPrefManager.KEY_EMAIL);
        usertoken = user.get(SharedPrefManager.KEY_TOKEN);

        edtemailuser = (EditText) findViewById(R.id.edtemailusers);
        edtpassword = (EditText) findViewById(R.id.edtpasswordusers);
        txtforgetpw = (TextView) findViewById(R.id.txtforgetmypw);
        txtregisteruser = (TextView) findViewById(R.id.txtregistermehere);
        btnLogin = (Button) findViewById(R.id.btnLoginUser);
        sharedPrefManager = new SharedPrefManager(getApplicationContext());

        if (sharedPrefManager.isLoggedIn()) {
            Intent i = new Intent(getApplicationContext(), TabView_maps_list.class);
            startActivity(i);
            finish();
        }

        btnLogin.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                if (event.getAction() == MotionEvent.ACTION_UP) {
                    String email_address = edtemailuser.getText().toString().trim();
                    String user_password = edtpassword.getText().toString().trim();

                    Intent intent = new Intent(LoginActivity.this, TabView_maps_list.class);
                    startActivity(intent);
                    finish();

                    /*if ((!email_address.isEmpty()) && (!user_password.isEmpty())) {
                        if (isEmailValid(email_address)) {
                            //sharedPrefManager.checkLogin();
                            loginUser(email_address, user_password);
                        } else {
                            edtemailuser.setError("Email Address is invalid");
                        }
                    } else if ((!email_address.isEmpty())) {
                        //Toast.makeText(getApplicationContext(),"Password is empty",Toast.LENGTH_LONG).show();
                        edtpassword.setError("Password is empty");
                    } else if ((!user_password.isEmpty())) {
                        //Toast.makeText(getApplicationContext(),"Email Address is empty",Toast.LENGTH_LONG).show();
                        edtemailuser.setError("Email Address is empty");
                    } else {
                        Toast.makeText(getApplicationContext(), "The fields are empty!", Toast.LENGTH_LONG).show();
                    }*/
                }
                return true;
            }
        });

        txtforgetpw.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                if (event.getAction() == MotionEvent.ACTION_UP) {
                    LayoutInflater layoutInflater = LayoutInflater.from(c);
                    View mView = layoutInflater.inflate(R.layout.customdialog_send_email, null);
                    AlertDialog.Builder alertDialogEmailInput = new AlertDialog.Builder(c);
                    alertDialogEmailInput.setView(mView);

                    final EditText edtEmailInput = (EditText) mView.findViewById(R.id.userInputDialog);
                    alertDialogEmailInput
                            .setCancelable(false)
                            .setPositiveButton("Send", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                }
                            })
                            .setNegativeButton("Cancel",
                                    new DialogInterface.OnClickListener() {
                                        @Override
                                        public void onClick(DialogInterface dialog, int which) {
                                            dialog.cancel();
                                        }
                                    });
                    AlertDialog alertDialogAndroid = alertDialogEmailInput.create();
                    alertDialogAndroid.show();
                    alertDialogAndroid.getButton(AlertDialog.BUTTON_POSITIVE).setOnTouchListener(new View.OnTouchListener() {
                        @Override
                        public boolean onTouch(View v, MotionEvent event) {
                            if (event.getAction() == MotionEvent.ACTION_UP) {
                                String edtemaillogin = edtEmailInput.getText().toString().trim();
                                if (!edtemaillogin.isEmpty()) {
                                    if (isEmailValid(edtemaillogin)) {
                                        requestForgetPassword(edtemaillogin);
                                    } else {
                                        //Toast.makeText(getApplicationContext(),"Email Address is invalid.",Toast.LENGTH_LONG).show();
                                        edtEmailInput.setError("Email Address is invalid");
                                    }
                                } else {
                                    //Toast.makeText(getApplicationContext(),"Email Address is empty",Toast.LENGTH_LONG).show();
                                    edtEmailInput.setError("Email Address is empty");
                                }
                            }
                            return true;
                        }
                    });
                }
                return true;
            }
        });

        txtregisteruser.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                if (event.getAction() == MotionEvent.ACTION_UP) {
                    Intent ii = new Intent(getApplicationContext(), RegisterWithEmail.class);
                    ii.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_NO_HISTORY);
                    startActivity(ii);
                    finish();
                }
                return true;
            }
        });
    }

    private void loginUser(final String email, final String password) {
        OkHttpClient.Builder builder = new OkHttpClient().newBuilder();
        builder.readTimeout(60, TimeUnit.SECONDS);
        builder.connectTimeout(60, TimeUnit.SECONDS);
        builder.writeTimeout(60, TimeUnit.SECONDS);
        if (BuildConfig.DEBUG) {
            HttpLoggingInterceptor interceptor = new HttpLoggingInterceptor();
            interceptor.setLevel(HttpLoggingInterceptor.Level.BODY);
            builder.addInterceptor(interceptor);
        }
        int cacheSize = 30 * 1024 * 1024; // 10 MiB
        Cache cache = new Cache(getCacheDir(), cacheSize);
        builder.cache(cache);

        //builder.interceptors().add(new Interceptor() {
        //    @Override
        //   public okhttp3.Response intercept(Chain chain) throws IOException {
        //       Request request = chain.request();
        //       Request.Builder requestBuilder = request.newBuilder();
        //       requestBuilder.addHeader("token","");
        //       request = requestBuilder.build();
        //      return chain.proceed(request);
        //   }
        // });

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(config.URL_BASE)
                .addConverterFactory(GsonConverterFactory.create())
                .client(builder.build())
                .build();

        RequestInterfaceRegistration requestInterfaceRegistration = retrofit.create(RequestInterfaceRegistration.class);

        Users users = new Users();
        users.setEmail(email);
        users.setUpassw(password);

        ServerRequest serverRequest = new ServerRequest();
        serverRequest.setEmail(email);
        serverRequest.setPassword(password);
        Call<ServerResponse> serverResponseCall = requestInterfaceRegistration.login("application/json", "", serverRequest);

        serverResponseCall.enqueue(new Callback<ServerResponse>() {
            @Override
            public void onResponse(Call<ServerResponse> call, Response<ServerResponse> response) {
                ServerResponse serverResponse = response.body();
                try {
                    if (serverResponse.getMessage().equals("User Logged-in Successfully !")) {
                        if (serverResponse.getResult().equals(config.MSG_SUCCESS)) {
                            //SharedPreferences preferences = getApplicationContext().getSharedPreferences("MyPref",0);
                            //SharedPreferences.Editor editor = preferences.edit();
                            //editor.putString(SharedPrefManager.KEY_TOKEN,serverResponse.getToken());
                            //editor.apply();
                            //editor.commit();

                            sharedPrefManager.createLoginSessionForUser(email, serverResponse.getToken());
                            Intent intent = new Intent(getApplicationContext(), TabView_maps_list.class);
                            //Bundle bundle = new Bundle();
                            //bundle.putString("token",serverResponse.getToken());
                            //bundle.putString("email",serverResponse.getEmail());
                            //Toast.makeText(getApplicationContext(),config.MSG_SUCCESS+serverResponse.getToken()+serverResponse.getEmail(),Toast.LENGTH_LONG).show();
                            //intent.putExtras(bundle);
                            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_NO_HISTORY);
                            startActivity(intent);
                            finish();
                        }
                    } else {
                        Toast.makeText(getApplicationContext(), config.MSG_FAILURE, Toast.LENGTH_LONG).show();
                    }
                } catch (Exception e) {
                    Toast.makeText(getApplicationContext(), "Error occured. Try again", Toast.LENGTH_LONG).show();
                }
            }

            @Override
            public void onFailure(Call<ServerResponse> call, Throwable t) {
                Log.d(config.TAG, "failed");
                Toast.makeText(getApplicationContext(), t.getMessage(), Toast.LENGTH_LONG).show();
            }
        });

    }

    private void requestForgetPassword(String email) {
        OkHttpClient.Builder builder = new OkHttpClient().newBuilder();
        builder.readTimeout(60, TimeUnit.SECONDS);
        builder.connectTimeout(60, TimeUnit.SECONDS);
        builder.writeTimeout(60, TimeUnit.SECONDS);
        if (BuildConfig.DEBUG) {
            HttpLoggingInterceptor interceptor = new HttpLoggingInterceptor();
            interceptor.setLevel(HttpLoggingInterceptor.Level.BODY);
            builder.addInterceptor(interceptor);
        }
        int cacheSize = 30 * 1024 * 1024; // 10 MiB
        Cache cache = new Cache(getCacheDir(), cacheSize);
        builder.cache(cache);

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(config.URL_BASE)
                .addConverterFactory(GsonConverterFactory.create())
                .client(builder.build())
                .build();

        RequestInterfaceRegistration requestInterfaceRegistration = retrofit.create(RequestInterfaceRegistration.class);

        Users users = new Users();
        users.setEmail(email);

        ServerRequest serverRequest = new ServerRequest();
        serverRequest.setEmail(email);
        Call<ServerResponse> serverResponseCall = requestInterfaceRegistration.forget("application/json", "", serverRequest);

        serverResponseCall.enqueue(new Callback<ServerResponse>() {
            @Override
            public void onResponse(Call<ServerResponse> call, Response<ServerResponse> response) {
                ServerResponse serverResponse = response.body();
                try {
                    if (serverResponse.getMessage().equals("Email Sent!")) {
                        if (serverResponse.getResult().equals(config.MSG_SUCCESS)) {
                            Intent ii = new Intent(getApplicationContext(), LoginActivity.class);
                            ii.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_NO_HISTORY);
                            startActivity(ii);
                            finish();
                        }
                    } else {
                        Toast.makeText(getApplicationContext(), config.MSG_FAILURE, Toast.LENGTH_LONG).show();
                    }
                } catch (Exception e) {
                    Toast.makeText(getApplicationContext(), "Error occurred. Try again", Toast.LENGTH_LONG).show();
                }

            }

            @Override
            public void onFailure(Call<ServerResponse> call, Throwable t) {
                Log.d(config.TAG, "failed");
                Toast.makeText(getApplicationContext(), t.getMessage(), Toast.LENGTH_LONG).show();
            }
        });
    }

    public static boolean isEmailValid(CharSequence target) {
        return !TextUtils.isEmpty(target) && Patterns.EMAIL_ADDRESS.matcher(target).matches();
    }

    @Override
    public boolean onTouch(View v, MotionEvent event) {
        return false;
    }

    @Override
    public void onBackPressed() {
        android.support.v7.app.AlertDialog.Builder builder = new android.support.v7.app.AlertDialog.Builder(this);
        builder.setMessage("Are you sure you want to exit?")
                .setCancelable(false)
                .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        System.exit(1);
                        //LoginActivity.this.finish();
                    }
                })
                .setNegativeButton("No", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.cancel();
                    }
                });
        android.support.v7.app.AlertDialog alert = builder.create();
        alert.show();

    }
}
