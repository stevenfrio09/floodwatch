package com.example.maryvianneybrion.floodwatchv3.requestinterpeys;

/**
 * Created by Mary Vianney Brion on 5/18/2017.
 */

import com.example.maryvianneybrion.floodwatchv3.model.JSONResponse;

import retrofit2.Call;
import retrofit2.http.GET;

public interface RequestMonitoringStatusInterface {
    @GET("FloodWatch/")
    Call<JSONResponse> getJson();
}
