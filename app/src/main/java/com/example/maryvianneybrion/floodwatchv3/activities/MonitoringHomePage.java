package com.example.maryvianneybrion.floodwatchv3.activities;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.CardView;
import android.support.v7.widget.Toolbar;
import android.view.MotionEvent;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.example.maryvianneybrion.floodwatchv3.R;

public class MonitoringHomePage extends AppCompatActivity implements View.OnTouchListener {

    Button btnHelp, btnSettings;
    TextView txtlocation, txtprevmonitoringstatus, txtdatedetailss, edtwaterlevel, edtmonitoringstatus;
    EditText edtUserLocation, txtdevice;
    CardView card_waterLevelStatus;
    private String usertoken = "";
    private String useremail = "";


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_monitoring_home_page);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        btnHelp = (Button) findViewById(R.id.btnHelps);
        btnSettings = (Button) findViewById(R.id.btnSettingss);
        txtdevice = (EditText) findViewById(R.id.txtDeviceDetails);
        txtdatedetailss = (TextView) findViewById(R.id.txtdatedetails);
        txtprevmonitoringstatus = (TextView) findViewById(R.id.txtpreviousmonitoring);
        edtmonitoringstatus = (TextView) findViewById(R.id.edtmonitoringstatus);
        edtwaterlevel = (TextView) findViewById(R.id.edtwaterlevels);
        edtUserLocation = (EditText) findViewById(R.id.editTextUserLocation);
        card_waterLevelStatus = (CardView) findViewById(R.id.card_waterLevelStatus);

        Bundle extras1 = getIntent().getExtras();


        //usertoken = extras1.getString("token");
        //useremail = extras1.getString("email");


        String messageMonitor = "Water level is normal.";
        String messagePrepare = "Build an emergency kit and make a family communications plan. Prepare for evacuation if water level rises. ";
        String messageEvacuate = "Move immediately to evacuation centers provided by government officials or to higher ground. ";


        Bundle bundle = getIntent().getExtras();


        if (bundle != null) {
            String height = bundle.getString("heightKey");


            String location = bundle.getString("locationKey");
            edtUserLocation.setText(location);

            String device = bundle.getString("deviceKey");
            txtdevice.setText(device);

            String status = bundle.getString("statusKey");
            if (status != null) {

                edtwaterlevel.setText(height + " ft" + ". " + status);


                if (status.equals("MONITOR")) {

                    card_waterLevelStatus.setCardBackgroundColor(ContextCompat.getColor(this, R.color.green));
                    edtmonitoringstatus.setText(messageMonitor);

                } else if (status.equals("PREPARE")) {

                    card_waterLevelStatus.setCardBackgroundColor(ContextCompat.getColor(this, R.color.yellow));
                    edtmonitoringstatus.setText(messagePrepare);

                } else if (status.equals("EVACUATE")) {

                    card_waterLevelStatus.setCardBackgroundColor(ContextCompat.getColor(this, R.color.red));
                    edtmonitoringstatus.setText(messageEvacuate);
                } else {
                    Toast.makeText(this, "null", Toast.LENGTH_SHORT).show();
                }


            }


            String updatedAt = bundle.getString("updatedAtKey");
            txtdatedetailss.setText("As of " + updatedAt);

        }
        //DateFormat dateTime = new SimpleDateFormat("'As of' hh:mm a MMM d yyyy");

        //String asOfTimeAndDate = dateTime.format(Calendar.getInstance().getTime());


        txtprevmonitoringstatus.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                if (event.getAction() == MotionEvent.ACTION_UP) {
                    Intent ii = new Intent(getApplicationContext(), TabView_maps_list.class);
                    startActivity(ii);

                }
                return true;
            }
        });

        btnHelp.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                if (event.getAction() == MotionEvent.ACTION_UP) {
                    Intent ii = new Intent(getApplicationContext(), HelpPage.class);
                    startActivity(ii);
                }
                return true;
            }
        });

        btnSettings.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                if (event.getAction() == MotionEvent.ACTION_UP) {
                    Intent intent = new Intent(MonitoringHomePage.this, Settings.class);
                    Bundle bundle = new Bundle();
                    bundle.putString("token", usertoken);
                    bundle.putString("email", useremail);
                    intent.putExtras(bundle);
                    startActivity(intent);
                    finish();
                }
                return true;
            }
        });

        edtUserLocation.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                final int DRAWABLE_LEFT = 0;
                final int DRAWABLE_TOP = 1;
                final int DRAWABLE_RIGHT = 2;
                final int DRAWABLE_BOTTOM = 3;

                if (event.getAction() == MotionEvent.ACTION_UP) {
                    if (event.getRawX() >= (edtUserLocation.getRight() - edtUserLocation.getCompoundDrawables()[DRAWABLE_RIGHT].getBounds().width())) {
                        Intent ii = new Intent(getApplicationContext(), TabView_maps_list.class);
                        startActivity(ii);
                        finish();
                    }
                }
                return true;
            }
        });


    }


    @Override
    public boolean onTouch(View v, MotionEvent event) {
        return false;
    }

    @Override
    public void onBackPressed() {
        startActivity(new Intent(this, TabView_maps_list.class));
        finish();
        super.onBackPressed();


    }
}