package com.example.maryvianneybrion.floodwatchv3.requestinterpeys;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.Header;
import retrofit2.http.POST;

/**
 * Created by Mary Vianney Brion on 5/19/2017.
 */

public interface RequestInterfaceRegistration {
    //register
    @POST("/api/users")
    Call<ServerResponse> register(@Body ServerRequest request);

    //login
    @POST("/api/users/login")
    Call<ServerResponse> login(@Header("Content-Type") String type, @Header("Token") String token, @Body ServerRequest request);

    //update profile
    @POST("/api/users/update")
    Call<ServerResponse> update(@Header("Content-Type") String type, @Header("Token") String token, @Body ServerRequest request);

    //change password
    @POST("/api/users/change")
    Call<ServerResponse> change(@Header("Content-Type") String type, @Header("Token") String token, @Body ServerRequest request);

    //forgot password
    @POST("/api/users/forgot")
    Call<ServerResponse> forget(@Header("Content-Type") String type, @Header("Token") String token, @Body ServerRequest request);

    //logout user
    @POST("/api/users/logout")
    Call<ServerResponse> logoutuser(@Header("Content-Type") String type, @Header("Token") String token, @Body ServerRequest request);

    /**create location
     @POST("/api/users_connection/create") Call<ServerResponse> createlocation(@Header("Content-Type") String type, @Header("Token") String token, @Body ServerRequest request);

     //delete location
     @POST("/api/users_connection/destroy") Call<ServerResponse> deletelocation(@Header("Content-Type") String type, @Header("Token") String token, @Body ServerRequest request);

     //update location
     @POST("/api/users_connection/update") Call<ServerResponse> updatelocation(@Header("Content-Type") String type, @Header("Token") String token, @Body ServerRequest request);

     //view water level
     @POST("/api/water_level/show") Call<ServerResponse> viewwaterlevel(@Header("Content-Type") String type, @Header("Token") String token, @Body ServerRequest request);**/


}
