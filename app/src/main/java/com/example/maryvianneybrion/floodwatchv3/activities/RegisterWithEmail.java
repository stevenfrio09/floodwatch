package com.example.maryvianneybrion.floodwatchv3.activities;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.location.LocationManager;
import android.os.Build;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.util.Patterns;
import android.view.MotionEvent;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.example.maryvianneybrion.floodwatchv3.BuildConfig;
import com.example.maryvianneybrion.floodwatchv3.R;
import com.example.maryvianneybrion.floodwatchv3.helpers.SharedPrefManager;
import com.example.maryvianneybrion.floodwatchv3.helpers.config;
import com.example.maryvianneybrion.floodwatchv3.model.Users;
import com.example.maryvianneybrion.floodwatchv3.requestinterpeys.RequestInterfaceRegistration;
import com.example.maryvianneybrion.floodwatchv3.requestinterpeys.ServerRequest;
import com.example.maryvianneybrion.floodwatchv3.requestinterpeys.ServerResponse;
import com.google.firebase.iid.FirebaseInstanceId;

import java.util.concurrent.TimeUnit;

import okhttp3.Cache;
import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class RegisterWithEmail extends Activity implements View.OnTouchListener {
    Button btnStart;
    EditText edtemail, edtpassword, edtconfirmpassword;
    String dv_token = "";
    TextView txtloginhere;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_register_with_email);

        final LocationManager manager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
        if (!manager.isProviderEnabled(LocationManager.GPS_PROVIDER)) {
            builderAlertMessageNoGps();
        }

        SharedPrefManager sharedPrefManager = new SharedPrefManager(this);
        if (sharedPrefManager.isLoggedIn()) {
            Intent i = new Intent(getApplicationContext(), TabView_maps_list.class);
            i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_NO_HISTORY);
            startActivity(i);
            finish();
        }

        btnStart = (Button) findViewById(R.id.btnRegisterButton);

        edtemail = (EditText) findViewById(R.id.emailtxt);
        edtpassword = (EditText) findViewById(R.id.passwordtxt);
        edtconfirmpassword = (EditText) findViewById(R.id.confirmpasswordtxt);
        txtloginhere = (TextView) findViewById(R.id.txtlogmehere);

        final String devToken = FirebaseInstanceId.getInstance().getToken();
        saveToken(devToken);
        dv_token = SharedPrefManager.getInstance(this).getDeviceToken();


        btnStart.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                if (event.getAction() == MotionEvent.ACTION_UP) {
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
                        String edtemailadd = edtemail.getText().toString().trim();
                        String edtupass = edtpassword.getText().toString().trim();
                        String edtcupass = edtconfirmpassword.getText().toString().trim();
                        if (!(edtemailadd.isEmpty() || edtupass.isEmpty() || edtcupass.isEmpty())) {
                            if (edtupass.equalsIgnoreCase(edtcupass) && isEmailValid(edtemailadd)) {
                                if (edtupass.length() < 6 && edtcupass.length() < 6) {
                                    edtpassword.setError("Minimum of 6 characters password");
                                    edtconfirmpassword.setError("Minimum of 6 characters password");
                                } else {
                                    registerUser(edtemailadd, edtupass, edtcupass, dv_token);
                                }
                            } else {
                                Toast.makeText(getApplicationContext(), "Check your email address or check if password does match", Toast.LENGTH_LONG).show();
                            }
                        } else {
                            Toast.makeText(getApplicationContext(), "All fields are required", Toast.LENGTH_LONG).show();
                        }
                    }
                }
                return true;
            }
        });

        txtloginhere.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                if (event.getAction() == MotionEvent.ACTION_UP) {
                    Intent ii = new Intent(getApplicationContext(), LoginActivity.class);
                    //Intent ii = new Intent(getApplicationContext(), TabView_maps_list.class);
                    ii.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_NO_HISTORY);
                    startActivity(ii);
                    finish();
                }
                return true;
            }
        });
    }

    private void registerUser(String user_email, String user_password, String user_confirm_password, String user_dev_token) {
        OkHttpClient.Builder builder = new OkHttpClient().newBuilder();
        builder.readTimeout(60, TimeUnit.SECONDS);
        builder.connectTimeout(30, TimeUnit.SECONDS);
        builder.writeTimeout(60, TimeUnit.SECONDS);
        if (BuildConfig.DEBUG) {
            HttpLoggingInterceptor interceptor = new HttpLoggingInterceptor();
            interceptor.setLevel(HttpLoggingInterceptor.Level.BODY);
            builder.addInterceptor(interceptor);
        }
        int cacheSize = 30 * 1024 * 1024; // 10 MiB
        Cache cache = new Cache(getCacheDir(), cacheSize);
        builder.cache(cache);

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(config.URL_BASE)
                .client(builder.build())
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        RequestInterfaceRegistration requestInterfaceRegistration = retrofit.create(RequestInterfaceRegistration.class);

        Users users = new Users();
        users.setEmail(user_email);
        users.setUpassw(user_password);
        users.setConfirm_password(user_confirm_password);
        users.setDevice_token(user_dev_token);

        ServerRequest request = new ServerRequest();
        request.setEmail(user_email);
        request.setPassword(user_password);
        request.setPassword_confirmation(user_confirm_password);
        request.setDevice_token(user_dev_token);
        Call<ServerResponse> response = requestInterfaceRegistration.register(request);

        response.enqueue(new Callback<ServerResponse>() {
            @Override
            public void onResponse(Call<ServerResponse> call, Response<ServerResponse> response) {
                ServerResponse serverResponse = response.body();
                try {
                    if (serverResponse.getMessage().equals("User Registered Successfully !")) {
                        if (serverResponse.getResult().equals(config.MSG_SUCCESS)) {
                            Toast.makeText(getApplicationContext(), config.MSG_SUCCESS, Toast.LENGTH_LONG).show();
                            Intent ii = new Intent(getApplicationContext(), LoginActivity.class);
                            //ii.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK |Intent.FLAG_ACTIVITY_NO_HISTORY);
                            startActivity(ii);
                            finish();
                        }
                    } else {
                        Toast.makeText(getApplicationContext(), config.MSG_FAILURE, Toast.LENGTH_LONG).show();
                    }
                } catch (Exception e) {
                    Toast.makeText(getApplicationContext(), config.MSG_SUCCESS, Toast.LENGTH_LONG).show();
                }
            }

            @Override
            public void onFailure(Call<ServerResponse> call, Throwable t) {
                Log.d(config.TAG, "failed");
                Toast.makeText(getApplicationContext(), config.MSG_FAILURE, Toast.LENGTH_LONG).show();
            }
        });

    }

    public static boolean isEmailValid(CharSequence target) {
        return !TextUtils.isEmpty(target) && Patterns.EMAIL_ADDRESS.matcher(target).matches();
    }

    private void saveToken(String token) {
        SharedPrefManager.getInstance(getApplicationContext());
        SharedPrefManager.saveDeviceToken(token);
    }

    private void builderAlertMessageNoGps() {
        final AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setMessage("Location settings is turned off. Enable it?")
                .setCancelable(false)
                .setPositiveButton("OPEN GPS", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(@SuppressWarnings("unused") final DialogInterface dialog, @SuppressWarnings("unused") final int id) {
                        startActivity(new Intent(android.provider.Settings.ACTION_LOCATION_SOURCE_SETTINGS));
                    }
                })
                .setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(@SuppressWarnings("unused") final DialogInterface dialog, @SuppressWarnings("unused") final int id) {
                        dialog.cancel();
                    }
                });
        final AlertDialog alertDialog = builder.create();
        alertDialog.show();
    }

    @Override
    public boolean onTouch(View v, MotionEvent event) {
        return false;
    }

    @Override
    public void onBackPressed() {
        android.support.v7.app.AlertDialog.Builder builder = new android.support.v7.app.AlertDialog.Builder(this);
        builder.setMessage("Are you sure you want to exit?")
                .setCancelable(false)
                .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        //RegisterWithEmail.this.finish();
                        System.exit(0);
                    }
                })
                .setNegativeButton("No", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.cancel();
                    }
                });
        android.support.v7.app.AlertDialog alert = builder.create();
        alert.show();

    }
}
