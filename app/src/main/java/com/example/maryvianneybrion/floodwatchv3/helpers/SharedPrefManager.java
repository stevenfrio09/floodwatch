package com.example.maryvianneybrion.floodwatchv3.helpers;

import java.util.HashMap;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;

import com.example.maryvianneybrion.floodwatchv3.activities.LoginActivity;

/**
 * Created by Mary Vianney Brion on 5/19/2017.
 */

public class SharedPrefManager {
    private static SharedPrefManager mInstance;
    private SharedPreferences preferences;
    private Editor editor;
    private static Context _context;

    private static final String PREF_NAME = "MyPref";
    private static final String IS_LOGIN = "isLoggedIn";
    private static final String TAG_TOKEN = "tagToken";
    public static final String KEY_EMAIL = "email";
    public static final String KEY_TOKEN = "token";


    public SharedPrefManager(Context context) {
        _context = context;
        int PRIVATE_MODE = 0;
        preferences = _context.getSharedPreferences(PREF_NAME, PRIVATE_MODE);
        editor = preferences.edit();
    }

    /**
     * Create Login session for user
     **/

    public void createLoginSessionForUser(String email, String token) {
        editor.putBoolean(IS_LOGIN, true);
        editor.putString(KEY_EMAIL, email);
        editor.putString(KEY_TOKEN, token);
        editor.commit();
    }

    /**
     * check whether the user is logged in or not
     **/

    public void checkLogin() {
        if (!this.isLoggedIn()) {
            Intent i = new Intent(_context, LoginActivity.class);
            i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            i.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            _context.startActivity(i);
        }
    }

    /**
     * Get stored data session
     **/

    public HashMap<String, String> getUserDetails() {
        HashMap<String, String> user = new HashMap<String, String>();
        user.put(KEY_EMAIL, preferences.getString(KEY_EMAIL, null));
        user.put(KEY_TOKEN, preferences.getString(KEY_TOKEN, null));
        return user;
    }

    /**
     * Clear session details
     */
    public void logoutUser() {
        editor.clear();
        editor.commit();
        Intent i = new Intent(_context, LoginActivity.class);
        i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        _context.startActivity(i);
    }

    /**
     * Quick check for login
     **/
    // Get Login State
    public boolean isLoggedIn() {
        return preferences.getBoolean(IS_LOGIN, false);
    }

    /***
     * Get Device Token
     * */

    public static boolean saveDeviceToken(String token) {
        SharedPreferences sharedPreferences = _context.getSharedPreferences(PREF_NAME, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putString(TAG_TOKEN, token);
        editor.apply();
        return true;
    }

    public String getDeviceToken() {
        SharedPreferences sharedPreferences = _context.getSharedPreferences(PREF_NAME, Context.MODE_PRIVATE);
        return sharedPreferences.getString(TAG_TOKEN, null);
    }


    public static SharedPrefManager getInstance(Context applicationContext) {
        if (mInstance == null) {
            mInstance = new SharedPrefManager(applicationContext);
            _context = applicationContext;
        }

        return mInstance;
    }
}
