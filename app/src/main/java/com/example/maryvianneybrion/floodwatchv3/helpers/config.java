package com.example.maryvianneybrion.floodwatchv3.helpers;

/**
 * Created by Mary Vianney Brion on 5/19/2017.
 */

public class config {

    public static final String URL_BASE = "http://192.168.43.156:3000/api/users/";

    public static final String MSG_SUCCESS = "success";
    public static final String MSG_FAILURE = "failed";

    public static final String IS_LOGGED_IN = "isLoggedIn";

    public static final String KEY_TOKEN_ONE = "";

    public static final String TAG = "FloodWatch";

}
