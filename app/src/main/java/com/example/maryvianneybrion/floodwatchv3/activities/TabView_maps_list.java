package com.example.maryvianneybrion.floodwatchv3.activities;

import android.Manifest;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Address;
import android.location.Geocoder;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.MenuItemCompat;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.Toast;

import com.android.volley.RequestQueue;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.example.maryvianneybrion.floodwatchv3.R;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapsInitializer;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class TabView_maps_list extends AppCompatActivity implements OnMapReadyCallback {

    FrameLayout displayMap, displayList;
    Button setMapView, setMonitoredListView;
    SearchView searchView;


    public RecyclerView recyclerView;
    public ArrayList<DataMonitored> data;
    public DataAdapter adapter;
    private SwipeRefreshLayout swipeRefreshLayout;

    List<DataMonitored> jsonResponse;

    private GoogleMap mGoogleMap;

    RequestQueue requestQueue;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_tab_view_maps_list);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);

        displayMap = (FrameLayout) findViewById(R.id.displayMap);
        displayList = (FrameLayout) findViewById(R.id.displayList);

        setMapView = (Button) findViewById(R.id.setMapView);
        setMonitoredListView = (Button) findViewById(R.id.setMonitoredListView);

        //initialize map
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.maps);
        mapFragment.getMapAsync(this);

        requestQueue = Volley.newRequestQueue(this);

        //set default view on onCreate
        setLayoutMapView();
        initViews();
        loadJSON();
        mapsVolleyRequest();


        setMapView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                setLayoutMapView();
            }
        });

        setMonitoredListView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                setLayoutMonitoredListView();
            }
        });


        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                loadJSON();
                mapsVolleyRequest();
            }
        });
    }

    public void setLayoutMapView() {

        setMapView.setBackgroundColor(ContextCompat.getColor(TabView_maps_list.this, R.color.colorPrimaryDark));
        setMonitoredListView.setBackgroundColor(ContextCompat.getColor(TabView_maps_list.this, android.R.color.transparent));

        displayMap.setVisibility(View.VISIBLE);
        displayMap.bringToFront();
        displayList.invalidate();


    }

    public void setLayoutMonitoredListView() {

        setMonitoredListView.setBackgroundColor(ContextCompat.getColor(TabView_maps_list.this, R.color.colorPrimaryDark));
        setMapView.setBackgroundColor(ContextCompat.getColor(TabView_maps_list.this, android.R.color.transparent));

        displayList.setVisibility(View.VISIBLE);
        displayList.bringToFront();
        displayMap.invalidate();


    }

    // INITIALIZE recyclerview
    public void initViews() {
        Log.d("FW", "ListActivity : initViews called");

        recyclerView = (RecyclerView) findViewById(R.id.card_recycler_view);
        recyclerView.setHasFixedSize(true);
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(getApplicationContext());
        recyclerView.setLayoutManager(layoutManager);

    }



    // fetch data and display to recyclerview

    public void loadJSON() {
        Log.d("FW", "ListActivity : loadJSON called");

        final ProgressDialog progressDialog = new ProgressDialog(this);
        progressDialog.setMessage("Fetching data from server...");
        progressDialog.setCancelable(true);
        progressDialog.show();

        Retrofit retrofit = new Retrofit.Builder()
                //.baseUrl("http://192.168.43.156:3000")
                .baseUrl("https://api.myjson.com")
                .addConverterFactory(GsonConverterFactory.create())
                .build();
        RequestInterface request = retrofit.create(RequestInterface.class);

        Call<List<DataMonitored>> call = request.getJSON();
        call.enqueue(new Callback<List<DataMonitored>>() {
            @Override
            public void onResponse(Call<List<DataMonitored>> call, Response<List<DataMonitored>> response) {
                Log.d("FW", "loadJSON : onResponse");

                if (response.isSuccess()) {
                    progressDialog.dismiss();
                    Log.d("FW", "loadJSON : onResponse : Success" + response);

                    jsonResponse = response.body();

                    setRecyclerViewItems();

                } else {
                    Log.d("FW", "loadJSON : onResponse : Failed");
                }
            }

            @Override
            public void onFailure(Call<List<DataMonitored>> call, Throwable t) {
                Log.d("FW", "loadJSON : onFailure called" + t.getMessage());
                progressDialog.dismiss();
                Toast.makeText(TabView_maps_list.this, t.getMessage(), Toast.LENGTH_SHORT).show();
            }
        });

    }

    public void setRecyclerViewItems() {
        data = new ArrayList<>(jsonResponse);
        adapter = new DataAdapter(data);
        adapter.notifyDataSetChanged();
        recyclerView.setAdapter(adapter);
    }


    // INITIALIZE toolbar
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {

        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu_action_bar, menu);
        MenuItem menuItem = menu.findItem(R.id.actionbar_search);
        searchView = (SearchView) MenuItemCompat.getActionView(menuItem);
        search(searchView);

        return super.onCreateOptionsMenu(menu);
    }

    private void search(SearchView searchView) {
        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                searchLocation();
                return true;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                try {
                    adapter.getFilter().filter(newText);
                }catch (NullPointerException e){

                }
                return true;

            }
        });
    }


    // SET ONCLICK ON TOOLBAR
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()) {
            case R.id.actionbar_help:

                Intent intentHelp = new Intent(TabView_maps_list.this, HelpPage.class);
                startActivity(intentHelp);
                finish();

                return true;
            case R.id.actionbar_settings:
                Intent intentSettings = new Intent(TabView_maps_list.this, Settings.class);
                startActivity(intentSettings);
                return true;

        }

        return super.onOptionsItemSelected(item);
    }


    /////// MAPS API BEGINS HERE

    @Override
    public void onMapReady(final GoogleMap googleMap) {
        MapsInitializer.initialize(this);
        mGoogleMap = googleMap;

        googleMap.setMapType(GoogleMap.MAP_TYPE_NORMAL);

        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling
            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.
            return;
        }
        googleMap.setMyLocationEnabled(true);
    }


    //searching location
    public void searchLocation() {
        String locationInput = searchView.getQuery().toString();
        Log.d("FW", "SEARCH VIEW INPUT IS : " + locationInput);

        List<Address> addressList = null;

        if (locationInput != null || !locationInput.equals("")) {
            Geocoder geocoder = new Geocoder(this);

            try {
                addressList = geocoder.getFromLocationName(locationInput, 1);

            } catch (IOException e) {
                e.printStackTrace();
            }
            try {
                Address address = addressList.get(0);
                LatLng latLng = new LatLng(address.getLatitude(), address.getLongitude());
                mGoogleMap.animateCamera(CameraUpdateFactory.newLatLngZoom(latLng, 15.0f));
            } catch (NullPointerException e) {
                Toast.makeText(this, "Unable to locate. Check your connection", Toast.LENGTH_LONG).show();
            }
        }

    }

    public void mapsVolleyRequest() {

        final ProgressDialog progressDialog = new ProgressDialog(this);
        progressDialog.setMessage("Fetching data from server...");
        progressDialog.setCancelable(true);
        progressDialog.show();

        String JsonObjectURL = "https://api.myjson.com/bins/lsgp1";
        //String JsonObjectURL = "http://192.168.43.156:3000/api/water_level";

        StringRequest stringRequest = new StringRequest(JsonObjectURL, new com.android.volley.Response.Listener<String>() {
            @Override
            public void onResponse(String response) {

                try {

                    final JSONArray jsonArray = new JSONArray(response);
                    for (int i = 0; i < jsonArray.length(); i++) {
                        final JSONObject jsonObject = jsonArray.getJSONObject(i);

                        String strLocationName = jsonObject.getString("name");
                        String strLatitude = jsonObject.getString("latitude");
                        String strLongitude = jsonObject.getString("longitude");

                        JSONObject jsonDevice = jsonObject.getJSONObject("device");
                        String strDevice = jsonDevice.getString("name");

                        JSONObject jsonLevel = jsonDevice.getJSONObject("level");
                        String strLevelStatus = jsonLevel.getString("status");

                        LatLng latLng = new LatLng(Double.valueOf(strLatitude), Double.valueOf(strLongitude));

                        final MarkerOptions markerOptions = new MarkerOptions().position(latLng).title(strLocationName);

                        if (strLevelStatus.equals("MONITOR")) {
                            markerOptions.icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_GREEN));
                        } else if (strLevelStatus.equals("PREPARE")) {
                            markerOptions.icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_YELLOW));
                        } else {
                            markerOptions.icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_RED));
                        }


                        mGoogleMap.setOnInfoWindowClickListener(new GoogleMap.OnInfoWindowClickListener() {
                            @Override
                            public void onInfoWindowClick(Marker marker) {
                                String locationName = marker.getTitle().toString();

                                try {
                                    for (int i = 0; i < jsonArray.length(); i++) {
                                        final JSONObject jsonObject = jsonArray.getJSONObject(i);

                                        JSONObject jsonDevice = jsonObject.getJSONObject("device");
                                        String strDevice = jsonDevice.getString("name");

                                        JSONObject jsonLevel = jsonDevice.getJSONObject("level");
                                        String strLevelStatus = jsonLevel.getString("status");
                                        String strLevelUpdatedAt = jsonLevel.getString("updated_at");
                                        String strHeight = jsonLevel.getString("height");


                                        if (locationName.equals(jsonObject.getString("name"))) {

                                            Intent intent = new Intent(getApplicationContext(), MonitoringHomePage.class);
                                            Bundle bundle = new Bundle();
                                            bundle.putString("locationKey", locationName);
                                            bundle.putString("deviceKey", strDevice);
                                            bundle.putString("statusKey", strLevelStatus);
                                            bundle.putString("heightKey", strHeight);
                                            bundle.putString("updatedAtKey", strLevelUpdatedAt);
                                            intent.putExtras(bundle);
                                            startActivity(intent);

                                        }
                                    }
                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }

                            }
                        });

                        mGoogleMap.addMarker(markerOptions);
                        progressDialog.dismiss();

                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }, new com.android.volley.Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

            }
        });

        requestQueue.add(stringRequest);

    }

    @Override
    public void onBackPressed() {
        System.exit(1);

        AlertDialog.Builder builder;
        builder = new AlertDialog.Builder(this);
        builder.setTitle("Exit")
                .setMessage("Are you sure you want to exit")
                .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        System.exit(0);
                    }
                })
                .setNegativeButton("No", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {

                    }
                });


        super.onBackPressed();
    }

}
