package com.example.maryvianneybrion.floodwatchv3.activities;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.example.maryvianneybrion.floodwatchv3.BuildConfig;
import com.example.maryvianneybrion.floodwatchv3.R;
import com.example.maryvianneybrion.floodwatchv3.helpers.SharedPrefManager;
import com.example.maryvianneybrion.floodwatchv3.helpers.config;
import com.example.maryvianneybrion.floodwatchv3.model.Users;
import com.example.maryvianneybrion.floodwatchv3.requestinterpeys.RequestInterfaceRegistration;
import com.example.maryvianneybrion.floodwatchv3.requestinterpeys.ServerRequest;
import com.example.maryvianneybrion.floodwatchv3.requestinterpeys.ServerResponse;

import java.util.HashMap;
import java.util.concurrent.TimeUnit;

import okhttp3.Cache;
import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;


public class Settings extends AppCompatActivity implements View.OnTouchListener {
    //private ListView lvsettings;
    private Button btnAbout, btnLogout, btnAccountSettings, btnchgpw;
    private SharedPrefManager sharedPrefManager;
    private String usertoken, useremail;
    final Context c = this;

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_settings);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        btnAbout = (Button) findViewById(R.id.btnAbout);
        btnLogout = (Button) findViewById(R.id.btnLogout);
        btnAccountSettings = (Button) findViewById(R.id.btnAccountSettings);
        btnchgpw = (Button) findViewById(R.id.btnChgPw);

        sharedPrefManager = new SharedPrefManager(getApplicationContext());
        HashMap<String, String> user = sharedPrefManager.getUserDetails();
        useremail = user.get(SharedPrefManager.KEY_EMAIL);
        usertoken = user.get(SharedPrefManager.KEY_TOKEN);
        //Toast.makeText(getApplicationContext(),useremail+usertoken,Toast.LENGTH_LONG).show();

        //Bundle extras1 = getIntent().getExtras();
        //usertoken = extras1.getString("token");
        //useremail = extras1.getString("email");

        btnAccountSettings.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                if (event.getAction() == MotionEvent.ACTION_UP) {
                    //Toast.makeText(getApplicationContext(),"Account Settings selected",Toast.LENGTH_LONG).show();
                    Intent ii = new Intent(getApplicationContext(), AccountSettings.class);
                    //startActivity(ii);
                    //finish();
                    //Intent intent = new Intent(Settings.this,AccountSettings.class);
                    //Bundle bundle = new Bundle();
                    //bundle.putString("token",usertoken);
                    //bundle.putString("email",useremail);
                    //intent.putExtras(bundle);
                    ii.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    startActivity(ii);
                }
                return true;
            }
        });

        btnchgpw.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                if (event.getAction() == MotionEvent.ACTION_UP) {
                    LayoutInflater layoutInflater = LayoutInflater.from(c);
                    View mView = layoutInflater.inflate(R.layout.change_password_settings_layout, null);
                    AlertDialog.Builder alertDialogChangePassword = new AlertDialog.Builder(c);
                    alertDialogChangePassword.setView(mView);

                    final EditText edtOldPassword = (EditText) mView.findViewById(R.id.userInputOldPassword);
                    final EditText edtNewPassword = (EditText) mView.findViewById(R.id.userInputNewPassword);
                    final EditText edtConfirmPassword = (EditText) mView.findViewById(R.id.userInputConfirmNewPassword);

                    alertDialogChangePassword
                            .setCancelable(false)
                            .setPositiveButton("Change Password", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                }
                            })
                            .setNegativeButton("Cancel",
                                    new DialogInterface.OnClickListener() {
                                        @Override
                                        public void onClick(DialogInterface dialog, int which) {
                                            dialog.cancel();
                                        }
                                    });
                    AlertDialog alertDialogAndroid = alertDialogChangePassword.create();
                    alertDialogAndroid.show();
                    alertDialogAndroid.getButton(AlertDialog.BUTTON_POSITIVE).setOnTouchListener(new View.OnTouchListener() {
                        @Override
                        public boolean onTouch(View v, MotionEvent event) {
                            if (event.getAction() == MotionEvent.ACTION_UP) {
                                final String old_pass = edtOldPassword.getText().toString().trim();
                                final String new_pass = edtNewPassword.getText().toString().trim();
                                final String confirm_pass = edtConfirmPassword.getText().toString().trim();
                                if (!old_pass.isEmpty() && !new_pass.isEmpty() && !confirm_pass.isEmpty()) {
                                    if (new_pass.equalsIgnoreCase(confirm_pass)) {
                                        if (new_pass.length() < 6 && confirm_pass.length() < 6 && old_pass.length() < 6) {
                                            edtOldPassword.setError("Minimum length of 6 characters");
                                            edtNewPassword.setError("Minimum length of 6 characters");
                                            edtConfirmPassword.setError("Minimum length of 6 characters");
                                        } else {
                                            //Toast.makeText(getApplicationContext(),usertoken+useremail+new_pass,Toast.LENGTH_LONG).show();
                                            changeUserPw(useremail, old_pass, new_pass, confirm_pass, usertoken);
                                            sharedPrefManager.logoutUser();
                                        }
                                    } else {
                                        Toast.makeText(getApplicationContext(), "Password does not match", Toast.LENGTH_LONG).show();
                                    }
                                } else {
                                    edtNewPassword.setError("New password is empty");
                                    edtConfirmPassword.setError("Confirm password is empty");
                                }
                            }
                            return true;
                        }
                    });
                }
                return true;
            }
        });

        btnAbout.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                if (event.getAction() == MotionEvent.ACTION_UP) {
                    Intent ii = new Intent(getApplicationContext(), AboutActivity.class);
                    ii.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_NO_HISTORY);
                    startActivity(ii);
                }
                return true;
            }
        });


        btnLogout.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                if (event.getAction() == MotionEvent.ACTION_UP) {
                    //sharedPrefManager.logoutUser();
                    //Intent ii = new Intent(getApplicationContext(),LoginActivity.class);
                    //startActivity(ii);
                    //finish();
                    logoutUser(usertoken);
                }
                return true;
            }
        });

    }

    private void changeUserPw(String email, String password, String newpassword, String newpassword_confirm, String token) {
        OkHttpClient.Builder builder = new OkHttpClient().newBuilder();
        builder.readTimeout(60, TimeUnit.SECONDS);
        builder.connectTimeout(60, TimeUnit.SECONDS);
        builder.writeTimeout(60, TimeUnit.SECONDS);
        if (BuildConfig.DEBUG) {
            HttpLoggingInterceptor interceptor = new HttpLoggingInterceptor();
            interceptor.setLevel(HttpLoggingInterceptor.Level.BODY);
            builder.addInterceptor(interceptor);
        }
        int cacheSize = 30 * 1024 * 1024; // 10 MiB
        Cache cache = new Cache(getCacheDir(), cacheSize);
        builder.cache(cache);

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(config.URL_BASE)
                .addConverterFactory(GsonConverterFactory.create())
                .client(builder.build())
                .build();

        RequestInterfaceRegistration requestInterfaceRegistration = retrofit.create(RequestInterfaceRegistration.class);

        Users users = new Users();
        users.setEmail(email);
        users.setNew_password(password);
        users.setNewpassword(newpassword);
        users.setNewpassword_confirmation(newpassword_confirm);

        ServerRequest serverRequest = new ServerRequest();
        serverRequest.setEmail(email);
        serverRequest.setPassword(password);
        serverRequest.setNewpassword(newpassword);
        serverRequest.setNewpassword_confirmation(newpassword_confirm);
        Call<ServerResponse> serverResponseCall = requestInterfaceRegistration.change("application/json", token, serverRequest);
        serverResponseCall.enqueue(new Callback<ServerResponse>() {
            @Override
            public void onResponse(Call<ServerResponse> call, Response<ServerResponse> response) {
                ServerResponse serverResponse = response.body();
                try {
                    if (serverResponse.getMessage().equals("Password Successfully Updated!")) {
                        if (serverResponse.getResult().equals(config.MSG_FAILURE)) {
                            Toast.makeText(getApplicationContext(), config.MSG_FAILURE, Toast.LENGTH_LONG).show();
                        } else {
                            sharedPrefManager.logoutUser();
                            Intent ii = new Intent(getApplicationContext(), LoginActivity.class);
                            ii.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_NO_HISTORY);
                            startActivity(ii);
                        }
                    } else {
                        Toast.makeText(getApplicationContext(), "Invalid Email! Try again", Toast.LENGTH_LONG).show();
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                    Toast.makeText(getApplicationContext(), "Error occured. Try again", Toast.LENGTH_LONG).show();
                }
            }

            @Override
            public void onFailure(Call<ServerResponse> call, Throwable t) {
                Log.d(config.TAG, "failed");
                Toast.makeText(getApplicationContext(), "There is an error occured while changing password. Please try again", Toast.LENGTH_LONG).show();
            }
        });
    }

    private void logoutUser(String token) {
        OkHttpClient.Builder builder = new OkHttpClient().newBuilder();
        builder.readTimeout(60, TimeUnit.SECONDS);
        builder.connectTimeout(60, TimeUnit.SECONDS);
        builder.writeTimeout(60, TimeUnit.SECONDS);
        if (BuildConfig.DEBUG) {
            HttpLoggingInterceptor interceptor = new HttpLoggingInterceptor();
            interceptor.setLevel(HttpLoggingInterceptor.Level.BODY);
            builder.addInterceptor(interceptor);
        }
        int cacheSize = 30 * 1024 * 1024; // 10 MiB
        Cache cache = new Cache(getCacheDir(), cacheSize);
        builder.cache(cache);

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(config.URL_BASE)
                .client(builder.build())
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        RequestInterfaceRegistration requestInterfaceRegistration = retrofit.create(RequestInterfaceRegistration.class);

        Users users = new Users();
        users.setToken(token);

        ServerRequest request = new ServerRequest();
        request.setToken(token);

        Call<ServerResponse> response = requestInterfaceRegistration.logoutuser("application/json", token, request);

        response.enqueue(new Callback<ServerResponse>() {
            @Override
            public void onResponse(Call<ServerResponse> call, Response<ServerResponse> response) {
                ServerResponse serverResponse = response.body();
                try {
                    if (serverResponse.getMessage().equals("Logged-out! !")) {
                        if (serverResponse.getResult().equals(config.MSG_SUCCESS)) {
                            try {
                                sharedPrefManager.logoutUser();
                                Toast.makeText(getApplicationContext(), config.MSG_SUCCESS, Toast.LENGTH_LONG).show();
                                Intent ii = new Intent(getApplicationContext(), LoginActivity.class);
                                ii.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP);
                                startActivity(ii);
                                finish();
                            } catch (Exception e) {
                                e.printStackTrace();
                                Toast.makeText(getApplicationContext(), config.MSG_FAILURE, Toast.LENGTH_LONG).show();
                            }
                        }
                    } else {
                        Toast.makeText(getApplicationContext(), config.MSG_FAILURE, Toast.LENGTH_LONG).show();
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                    Toast.makeText(getApplicationContext(), "Error occured. Try again", Toast.LENGTH_LONG).show();
                }

            }

            @Override
            public void onFailure(Call<ServerResponse> call, Throwable t) {
                Log.d(config.TAG, "failed");
                Toast.makeText(getApplicationContext(), config.MSG_FAILURE, Toast.LENGTH_LONG).show();
            }
        });
    }

    @Override
    public boolean onTouch(View v, MotionEvent event) {
        return false;
    }

}
